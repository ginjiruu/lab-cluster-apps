# Lab Cluster Apps

This repo is a collection of all kubernetes apps that run on the "lab-cluster" k3s cluster

## Adding a new app

create a directory named after the app you want to create and copy the template file into it. 
Modify as necessary